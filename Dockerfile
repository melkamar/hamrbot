FROM php:8.1.11-zts-buster
WORKDIR /app

RUN apt-get update && \
    apt-get install -y build-essential autoconf libzip-dev chromium-driver libssl-dev pkg-config && \
    pecl install mongodb zip && \
    apt-get purge -y build-essential autoconf libzip-dev libssl-dev pkg-config && \
    curl -LO https://getcomposer.org/installer && \
    php installer && \
    mv composer.phar /usr/local/bin/composer && \
    echo 'extension=mongodb.so' > /usr/local/etc/php/conf.d/ext.ini && \
    echo 'extension=zip.so'    >> /usr/local/etc/php/conf.d/ext.ini

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash && \
    apt install -y symfony-cli


COPY . .
RUN composer install

CMD ["bin/console", "watchdog:process"]
