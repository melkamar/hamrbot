build:
	docker-compose -f docker-compose.yml -f docker-compose.prod.local.yml build

deploy:
	docker-compose -f docker-compose.yml -f docker-compose.prod.local.yml up -d
	docker-compose -f docker-compose.yml -f docker-compose.prod.local.yml ps

logs:
	docker-compose -f docker-compose.yml -f docker-compose.prod.local.yml logs -f
