<?php
declare(strict_types=1);


namespace App\Command;


use App\Document\TimeSlot;
use App\Document\Watchdog;
use App\Service\Repository\WatchdogRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('watchdog:add')]
class AddWatchdogCommand extends Command
{
    private readonly WatchdogRepository $repository;

    public function __construct(
        private DocumentManager $dm,
        string                  $name = null,
    )
    {
        parent::__construct($name);
        $this->repository = $dm->getRepository(Watchdog::class);
    }

    protected
    function configure()
    {
        parent::configure();

        $this->addArgument('DAY', mode: InputArgument::REQUIRED, description: 'The day for when to create the watchdog. Format dd.mm.yyyy.');
        $this->addArgument('FROM', mode: InputArgument::REQUIRED, description: 'The start of the time slot for the watchdog. Format HH:MM, allowed 07:00-22:30 and only full or half-hours.');
        $this->addArgument('TO', mode: InputArgument::REQUIRED, description: 'The start of the time slot for the watchdog. Format HH:MM, allowed 07:00-22:30 and only full or half-hours.');
        $this->addArgument('EMAIL', mode: InputArgument::REQUIRED, description: 'E-mail address where to send a notification when the time is available.');
    }

    protected
    function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument('EMAIL');
        $dog = new Watchdog(
            new TimeSlot(
                $input->getArgument('DAY'),
                $input->getArgument('FROM'),
                $input->getArgument('TO'),
            ),
            $email
        );

        $this->repository->save($dog);

        $io = new SymfonyStyle($input, $output);
        $io->success("Created a watchdog for $email at $dog->timeSlot.");

        return Command::SUCCESS;
    }


}