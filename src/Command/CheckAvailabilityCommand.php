<?php
declare(strict_types=1);


namespace App\Command;

use App\Document\TimeSlot;
use App\Exception\InvalidTimeSlotException;
use App\Service\HamrFetcher;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'check-slot')]
class CheckAvailabilityCommand extends Command
{
    protected SymfonyStyle $io;

    public function __construct(private readonly HamrFetcher $hamrFetcher)
    {
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->addArgument('DAY');
        $this->addArgument('FROM');
        $this->addArgument('TO');
    }


    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);

        $this->io = new SymfonyStyle($input, $output);
    }


    /**
     * @throws InvalidTimeSlotException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $day = $input->getArgument('DAY');
        $from = $input->getArgument('FROM');
        $to = $input->getArgument('TO');

        $timetable = $this->hamrFetcher->fetchTimetable();

        if ($timetable->isAvailable(new TimeSlot($day, $from, $to))) {
            $this->io->success("Time slot on $day $from-$to is available.");
            return Command::SUCCESS;
        }

        $this->io->warning("Time slot on $day $from-$to is not available.");
        return Command::FAILURE;
    }


}