<?php
declare(strict_types=1);


namespace App\Command;


use App\Service\WatchdogProcessor;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'watchdog:process')]
class ProcessWatchdogsCommand extends Command
{
    public function __construct(
        private readonly WatchdogProcessor $processor,
        string                             $name = null,
    )
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->processor->processWatchdogs();

        return Command::SUCCESS;
    }
}