<?php
declare(strict_types=1);


namespace App\Controller;


use App\Document\Watchdog;
use App\Service\Repository\WatchdogRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArchiveController extends AbstractController
{
    private readonly WatchdogRepository $watchdogRepository;

    public function __construct(
        DocumentManager $dm
    )
    {
        $this->watchdogRepository = $dm->getRepository(Watchdog::class);
    }

    #[Route('/watchdog/archive')]
    public function history(): Response
    {
        return $this->render(
            'root/watchdog_archive.html.twig',
            [
                'watchdogs' => $this->watchdogRepository->getNonActiveWatchdogs(),
            ]
        );
    }
}