<?php
declare(strict_types=1);


namespace App\Controller;


use App\Document\TimeSlot;
use App\Document\Watchdog;
use App\Document\WatchdogProcessingRecord;
use App\Service\Repository\WatchdogProcessRecordRepository;
use App\Service\Repository\WatchdogRepository;
use App\Service\WatchdogProcessor;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    private const FLASH_ERROR = 'error';
    private const FLASH_NOTICE = 'notice';
    private const FLASH_SUCCESS = 'success';

    private readonly WatchdogRepository $watchdogRepository;
    private readonly WatchdogProcessRecordRepository $watchdogProcessRecordRepository;

    public function __construct(
        DocumentManager           $dm,
        private WatchdogProcessor $watchdogProcessor
    )
    {
        $this->watchdogRepository = $dm->getRepository(Watchdog::class);
        $this->watchdogProcessRecordRepository = $dm->getRepository(WatchdogProcessingRecord::class);
    }

    #[Route('/')]
    public function index(): Response
    {
        return $this->render(
            'root/root.html.twig',
            [
                'watchdogs' => $this->getActiveWatchdogs(),
                'lastWatchdogProcessRecords' => $this->watchdogProcessRecordRepository->getNewestProcessRecords(10),
            ]
        );
    }

    #[Route('/watchdog/new')]
    public function newWatchdog(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('date', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('timeFrom', ChoiceType::class, [
                'choices' => TimeSlot::SLOT_TIME_TO_IDX,
            ])
            ->add('timeTo', ChoiceType::class, [
                'choices' => TimeSlot::SLOT_TIME_TO_IDX,
            ])
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Vytvořit hlídacího psa'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $watchdog = new Watchdog(
                new TimeSlot(
                    $data['date'],
                    TimeSlot::timeIdxToString($data['timeFrom']),
                    TimeSlot::timeIdxToString($data['timeTo']),
                ),
                $data['email'],
            );
            $this->watchdogRepository->save($watchdog);
            $this->addFlashSuccess("Hlídací pes {$watchdog->timeSlot} přidán");

            return $this->redirectToRoute('app_index_index');
        }

        return $this->renderForm('root/add_watchdog.html.twig', ['form' => $form]);
    }

    #[Route('/watchdog/delete/{id}', methods: ['POST'])]
    public function deleteWatchdog(string $id)
    {
        /** @var Watchdog $watchdog */
        $watchdog = $this->watchdogRepository->find($id);

        if ($watchdog === null) {
            $this->addFlash(
                'notice',
                "Hlídací pes #{$id} nenalezen"
            );
        } else {
            $this->watchdogRepository->delete($watchdog);

            $this->addFlash(
                'notice',
                "Hlídací pes {$watchdog->timeSlot} smazán"
            );
        }

        return $this->redirectToRoute('app_index_index');
    }

    #[Route('/watchdog/process', methods: ['POST'])]
    public function processWatchdogsAction(Request $request)
    {
        $this->watchdogProcessor->processWatchdogs();

        $this->addFlashSuccess('Hlídací psi zkontrolováni');

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    private function getActiveWatchdogs(): array
    {
        return $this->watchdogRepository->getActiveNonFiredWatchdogs();
    }

    private function addFlashSuccess(mixed $message): void
    {
        $this->addFlash(self::FLASH_SUCCESS, $message);
    }

    private function addFlashNotice(mixed $message): void
    {
        $this->addFlash(self::FLASH_NOTICE, $message);
    }

    private function addFlashError(mixed $message): void
    {
        $this->addFlash(self::FLASH_ERROR, $message);
    }
}