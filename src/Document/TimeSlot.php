<?php
declare(strict_types=1);


namespace App\Document;


use App\Exception\InvalidTimeSlotException;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

#[EmbeddedDocument]
class TimeSlot
{
    public const SLOT_TIME_TO_IDX = [
        '07:00' => 0,
        '07:30' => 1,
        '08:00' => 2,
        '08:30' => 3,
        '09:00' => 4,
        '09:30' => 5,
        '10:00' => 6,
        '10:30' => 7,
        '11:00' => 8,
        '11:30' => 9,
        '12:00' => 10,
        '12:30' => 11,
        '13:00' => 12,
        '13:30' => 13,
        '14:00' => 14,
        '14:30' => 15,
        '15:00' => 16,
        '15:30' => 17,
        '16:00' => 18,
        '16:30' => 19,
        '17:00' => 20,
        '17:30' => 21,
        '18:00' => 22,
        '18:30' => 23,
        '19:00' => 24,
        '19:30' => 25,
        '20:00' => 26,
        '20:30' => 27,
        '21:00' => 28,
        '21:30' => 29,
        '22:00' => 30,
        '22:30' => 31,
    ];

    public function __construct(
        #[Field(type: 'date')] public readonly \DateTime $date,
        #[Field(type: 'string')] public readonly string    $timeFrom,
        #[Field(type: 'string')] public readonly string    $timeTo,
    )
    {
        if (!array_key_exists($this->timeFrom, self::SLOT_TIME_TO_IDX) || !array_key_exists($this->timeTo, self::SLOT_TIME_TO_IDX)) {
            throw new InvalidTimeSlotException("Time slot \"$timeFrom\" does not exist. Choose e.g. \"07:00\" or \"07:30\" etc.");
        }

        if (self::SLOT_TIME_TO_IDX[$this->timeFrom] >= self::SLOT_TIME_TO_IDX[$this->timeTo]) {
            throw new InvalidTimeSlotException("Time from ($this->timeFrom) must be earlier than time to ($this->timeTo).");
        }
    }

    public static function getIdxTimeFrom(string $timeFrom): int
    {
        if (!array_key_exists($timeFrom, TimeSlot::SLOT_TIME_TO_IDX)) {
            throw new InvalidTimeSlotException("Time slot \"$timeFrom\" does not exist. Choose e.g. \"07:00\" or \"07:30\" etc.");
        }

        return TimeSlot::SLOT_TIME_TO_IDX[$timeFrom];
    }

    public static function getIdxTimeTo(string $timeTo): int
    {
        $result = self::getIdxTimeFrom($timeTo) - 1;
        if ($result < 0) {
            throw new InvalidTimeSlotException("Time cannot end at the first time slot.");
        }

        return $result;
    }

    public static function timeIdxToString(int $idx): string
    {
        $flipped = \array_flip(self::SLOT_TIME_TO_IDX); // todo cache
        if (!\array_key_exists($idx, $flipped)) {
            throw new InvalidTimeSlotException("Index \"$idx\" cannot be mapped to any time slot.");
        }

        return $flipped[$idx];
    }

    public function isInThePast(): bool
    {
        return new \DateTime() > $this->date;
    }

    public function __toString(): string
    {
        $dateStr = $this->date->format('d.m.Y');
        return "[$dateStr $this->timeFrom-$this->timeTo]";
    }
}
