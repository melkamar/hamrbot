<?php
declare(strict_types=1);


namespace App\Document;


use App\Exception\CannotActivateWatchdogException;
use App\Service\Repository\WatchdogRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use MongoDB\BSON\ObjectId;


#[Document(repositoryClass: WatchdogRepository::class)]
class Watchdog
{
    #[Id]
    public readonly string $id;

    #[Field(type: 'date')]
    public ?\DateTime $firedAt = null;

    public function __construct(
        #[EmbedOne(targetDocument: TimeSlot::class)] public readonly TimeSlot $timeSlot,
        #[Field(type: 'string')] public readonly string                       $email,
        #[Field(type: 'bool')] private bool                                   $isActive = true,
        ?\DateTime                                                            $firedAt = null
    )
    {
        $this->id = (string)new ObjectId();
        $this->firedAt = $firedAt;
    }

    public function markAsFired()
    {
        $this->firedAt = new \DateTime();
        $this->isActive = false;
    }

    public function getFiredAt(): \DateTime
    {
        return $this->firedAt;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function activate(): void
    {
        if ($this->timeSlot->isInThePast()) {
            throw new CannotActivateWatchdogException("Cannot activate a watchdog because its timeslot \"{$this->timeSlot->date}\" is in the past.");
        }
        $this->isActive = true;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }
}