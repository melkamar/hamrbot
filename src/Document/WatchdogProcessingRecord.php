<?php
declare(strict_types=1);


namespace App\Document;

use App\Service\Repository\WatchdogProcessRecordRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Index;
use MongoDB\BSON\ObjectId;

#[Document(
    collection: [
        'name' => 'WatchdogProcessingRecord',
        'capped' => true,
        'max' => 10000,
        'size' => 100000
    ],
    repositoryClass: WatchdogProcessRecordRepository::class
)]
class WatchdogProcessingRecord
{
    #[Id]
    public readonly string $id;

    public function __construct(
        #[Field(type: 'date')] #[Index] public readonly \DateTime $timestamp = new \DateTime()
    )
    {
        $this->id = (string)new ObjectId();
    }
}