<?php
declare(strict_types=1);


namespace App\Model;


use App\Document\TimeSlot;

class DaySlots
{

    /**
     * @param array<bool> $timeSlots
     */
    public function __construct(
        public readonly string $day,
        private readonly array $timeSlots
    )
    {
    }

    public function isAvailable(string $timeFrom, string $timeTo)
    {
        $idxFrom = TimeSlot::getIdxTimeFrom($timeFrom);
        $idxTo = TimeSlot::getIdxTimeTo($timeTo);

        for ($i = $idxFrom; $i <= $idxTo; $i++) {
            if ($this->timeSlots[$i] === false) {
                return false;
            }
        }

        return true;
    }
}