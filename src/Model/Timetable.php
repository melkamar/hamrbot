<?php
declare(strict_types=1);


namespace App\Model;


use App\Document\TimeSlot;
use App\Exception\InvalidTimeSlotException;

class Timetable
{
    /** @var array<string, DaySlots> */
    private array $daysToDaySlots;

    /**
     * @param string $screenshot screenshot of the timetable at the time where it is being parsed
     */
    public function __construct(
        array                  $allDaySlots,
        public readonly string $screenshot
    )
    {
        $this->daysToDaySlots = [];

        /** @var DaySlots $daySlots */
        foreach ($allDaySlots as $daySlots) {
            $this->daysToDaySlots[$daySlots->day] = $daySlots;
        }
    }

    public function isAvailable(TimeSlot $timeSlot): bool
    {
        if (!array_key_exists($timeSlot->date->format('d.m.Y'), $this->daysToDaySlots)) {
            throw new InvalidTimeSlotException("No results for day \"$timeSlot->date\"");
        }

        return $this->daysToDaySlots[$timeSlot->date->format('d.m.Y')]->isAvailable($timeSlot->timeFrom, $timeSlot->timeTo);
    }
}