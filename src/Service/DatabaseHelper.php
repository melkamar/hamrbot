<?php
declare(strict_types=1);


namespace App\Service;


use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\Driver\Exception\CommandException;

class DatabaseHelper
{
    public function __construct(
        private readonly DocumentManager $dm
    )
    {
    }

    public function createCollectionIfNotExists(string $documentName): void
    {
        try {
            $this->dm->getSchemaManager()->createDocumentCollection($documentName);
        } catch (CommandException $e) {
            if (str_contains($e->getMessage(), 'already exists')) {
                return;
            }

            throw $e;
        }

    }
}