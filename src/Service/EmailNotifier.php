<?php
declare(strict_types=1);


namespace App\Service;


use App\Document\Watchdog;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailNotifier
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly LoggerInterface $logger
    )
    {
    }

    /**
     * Send an email notification for the given watchdog.
     *
     * @param Watchdog $watchdog
     * @return bool true if the notification was sent, false if something failed
     */
    public function notifyForWatchdog(Watchdog $watchdog, string $screenshot): bool
    {
        $this->logger->debug("Sending e-mail notification for watchdog $watchdog->id to $watchdog->email");

        $email = new Email();
        $email
            ->subject('Volný termín v Hamru: ' . $watchdog->timeSlot)
            ->from('hamrbot.rezervace@gmail.com')
            ->to($watchdog->email)
            ->attach($screenshot, 'Rozvrh hodin', 'image/png')
            ->html(<<<EOF
<body>
<h3>Hurá, je volný termín na badminton!</h3>

<p>
Měl jsem upozornit na $watchdog->timeSlot
</p>

<p>
<a href="https://hodiny.hamrsport.cz">Rezervace tady</a>
</p>

Moje práce je hotova, už nebudu rušit.
</body>
EOF
            );

        try {
            $this->mailer->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            $this->logger->error("Failed to send e-mail notification for watchdog $watchdog->id: {$e->getMessage()}");
            return false;
        }
    }
}