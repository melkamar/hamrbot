<?php
declare(strict_types=1);


namespace App\Service;


use App\Model\DaySlots;
use App\Model\Timetable;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Panther\Client;

class HamrFetcher
{
    public function __construct(private KernelInterface $kernel)
    {
    }

    public function fetchTimetable(): Timetable
    {
        $args = [
            '--window-size=2160,1080',
            '--disable-dev-shm-usage',
            '--no-sandbox',
            '--headless',
        ];

        if (file_exists($this->kernel->getProjectDir() . '/drivers/chromedriver')) {
            $client = Client::createChromeClient(
                $this->kernel->getProjectDir() . '/drivers/chromedriver',
                $args
            );
        } else {
            $client = Client::createChromeClient(arguments: $args);
        }

        $crawler = $client->request('GET', 'https://hodiny.hamrsport.cz/Login.aspx'); // Yes, this website is 100% written in JavaScript

        $localityInput = $crawler->filterXPath(".//select[@id='ctl00_workspace_ddlLocality']//option[@value='171']");
        $localityInput->click();

        $client->waitForVisibility('#ctl00_workspace_ddlSport');

        $sportInput = $crawler->filterXPath(".//select[@id='ctl00_workspace_ddlSport']//option[@value='140']");
        $sportInput->click();

        $client->waitForVisibility('#ctl00_workspace_ReservationGrid_divResGrid');

        $reservationGrid = $crawler->filter('div#ctl00_workspace_ReservationGrid_divResGrid');

        $tableRows = $reservationGrid->filter('tr.rg-row');

        $parsedRows = $tableRows->each(
            function (Crawler $cell) {
                return $this->parseDaySlotsFromSingleRow($cell);
            }
        );

        $screenshot = $client->takeScreenshot('ss.png');

        return new Timetable($parsedRows, $screenshot);
    }

    private function parseDaySlotsFromSingleRow(Crawler $filteredRow): DaySlots
    {
        $fullDayText = $filteredRow->filter('td')->first()->text();
        [$_, $dateText] = \explode(' ', $fullDayText);

        $timeSlots = $filteredRow->filter('td.rg-item')->each(
            function (Crawler $cell) {
                if ($cell->filter('div.rgs-free')->count() === 1) {
                    return true;
                }

                if ($cell->filter('div.rgs-none')->count() === 1) {
                    return false;
                }

                throw new \LogicException('Could not parse a time slot element: ' . $cell->html());
            }
        );

        return new DaySlots($dateText, $timeSlots);
    }
}