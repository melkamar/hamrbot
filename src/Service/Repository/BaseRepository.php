<?php
declare(strict_types=1);


namespace App\Service\Repository;


use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

abstract class BaseRepository extends DocumentRepository
{
    public function save(object $obj)
    {
        $this->dm->persist($obj);
        $this->dm->flush();
    }

    public function delete(object $obj)
    {
        $this->dm->remove($obj);
        $this->dm->flush();
    }
}