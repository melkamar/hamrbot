<?php
declare(strict_types=1);


namespace App\Service\Repository;


use App\Document\WatchdogProcessingRecord;

class WatchdogProcessRecordRepository extends BaseRepository
{
    /**
     * @param int $numberOfRecords
     * @return array<WatchdogProcessingRecord>
     */
    public function getNewestProcessRecords(int $numberOfRecords): array
    {
        return $this->findBy(
            [],
            orderBy: ['timestamp' => -1],
            limit: $numberOfRecords
        );
    }
}