<?php
declare(strict_types=1);


namespace App\Service\Repository;


class WatchdogRepository extends BaseRepository
{
    public function getActiveNonFiredWatchdogs(): array
    {
        return $this->findBy(
            [
                'isActive' => true,
                'firedAt' => ['$exists' => false],
            ],
            [
                'timeSlot.date' => 1,
                'timeSlot.timeFrom' => 1,
                'timeSlot.timeTo' => 1,
            ]
        );
    }

    public function getNonActiveWatchdogs(): array
    {
        return $this->findBy(
            [
                '$or' => [
                    ['isActive' => false],
                    ['firedAt' => ['$exists' => true]],
                ],
            ],
            [
                'timeSlot.date' => -1,
                'timeSlot.timeFrom' => -1,
                'timeSlot.timeTo' => -1,
            ]
        );
    }
}