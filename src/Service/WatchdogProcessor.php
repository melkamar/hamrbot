<?php
declare(strict_types=1);


namespace App\Service;


use App\Document\Watchdog;
use App\Document\WatchdogProcessingRecord;
use App\Exception\InvalidTimeSlotException;
use App\Model\Timetable;
use App\Service\Repository\WatchdogRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;

class WatchdogProcessor
{
    private readonly WatchdogRepository $watchdogRepository;

    public function __construct(
        private DocumentManager          $dm,
        private readonly HamrFetcher     $fetcher,
        private readonly EmailNotifier   $emailNotifier,
        private readonly LoggerInterface $logger,
        private readonly DatabaseHelper  $databaseHelper
    )
    {
        $this->watchdogRepository = $dm->getRepository(Watchdog::class);
    }

    public function processWatchdogs(): void
    {
        $this->createWatchdogProcessingRecord();

        $activeWatchdogs = $this->watchdogRepository->getActiveNonFiredWatchdogs();
        $this->logger->info("There are " . \count($activeWatchdogs) . " active watchdogs");

        $currentAndActiveWatchdogs = $this->deactivateWatchdogsInThePast($activeWatchdogs);
        $this->logger->info("There are " . \count($currentAndActiveWatchdogs) . " watchdogs to process");

        if (\count($currentAndActiveWatchdogs) === 0) {
            $this->logger->info('There are no non-fired watchdogs to process.');
            return;
        }

        $timetable = $this->fetcher->fetchTimetable();

        foreach ($currentAndActiveWatchdogs as $watchdog) {
            $this->processWatchdog($timetable, $watchdog);
        }
        $this->watchdogRepository->getDocumentManager()->flush();
    }

    /**
     * @param array<Watchdog> $watchdogs
     * @return array<Watchdog>
     */
    private function deactivateWatchdogsInThePast(array $watchdogs): array
    {
        $activeWatchdogs = [];
        foreach ($watchdogs as $watchdog) {
            if ($watchdog->timeSlot->isInThePast()) {
                $watchdog->deactivate();
                $this->logger->info("Deactivating watchdog {$watchdog->id} because its time slot is in the past");
            } else {
                $activeWatchdogs[] = $watchdog;
            }
        }

        $this->watchdogRepository->getDocumentManager()->flush();

        return $activeWatchdogs;
    }

    private function processWatchdog(Timetable $timetable, Watchdog $watchdog): void
    {
        try {
            $isAvailable = $timetable->isAvailable($watchdog->timeSlot);
        } catch (InvalidTimeSlotException $e) {
            $this->logger->error('An error occurred when processing watchdog ' . $watchdog->id . ': ' . $e->getMessage());
            return;
        }

        if (!$isAvailable) {
            $this->logger->info("The time slot {$watchdog->timeSlot} is not available for watchdog {$watchdog->id}");
            return;
        }

        $this->logger->info("The time slot {$watchdog->timeSlot} is available for watchdog {$watchdog->id}");

        if ($this->emailNotifier->notifyForWatchdog($watchdog, $timetable->screenshot)) {
            $watchdog->markAsFired();
        }
    }

    private function createWatchdogProcessingRecord(): void
    {
        $this->databaseHelper->createCollectionIfNotExists(WatchdogProcessingRecord::class);

        $record = new WatchdogProcessingRecord();
        $this->dm->persist($record);
    }
}