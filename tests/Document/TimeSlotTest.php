<?php
declare(strict_types=1);


namespace App\Tests\Document;


use App\Document\TimeSlot;
use App\Exception\InvalidTimeSlotException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class TimeSlotTest extends TestCase
{
    public function testIsInThePastTrue(): void
    {
        $slot = new TimeSlot('06.06.2000', '10:00', '12:00');
        Assert::assertTrue($slot->isInThePast());
    }

    public function testIsInThePastFalse(): void
    {
        $slot = new TimeSlot('06.06.9999', '10:00', '12:00');
        Assert::assertFalse($slot->isInThePast());
    }

    public function testTimeToCannotBeSameAsTimeFrom(): void
    {
        $this->expectException(InvalidTimeSlotException::class);
        new TimeSlot('06.06.2000', '10:00', '10:00');
    }

    public function testTimeToCannotBeLowerThanTimeFrom(): void
    {
        $this->expectException(InvalidTimeSlotException::class);
        new TimeSlot('06.06.2000', '10:00', '09:00');
    }
}