<?php
declare(strict_types=1);


namespace App\Tests\Document;


use App\Document\TimeSlot;
use App\Document\Watchdog;
use App\Exception\CannotActivateWatchdogException;
use PHPUnit\Framework\TestCase;

class WatchdogTest extends TestCase
{
    public function testWatchdogCannotBeActivateWhenInThePast()
    {
        $dog = new Watchdog(new TimeSlot('01.01.2000', '10:00', '11:00'), 'fob@ar');

        $this->expectException(CannotActivateWatchdogException::class);
        $dog->activate();
    }
}